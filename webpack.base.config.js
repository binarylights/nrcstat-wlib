const fs = require('fs');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin')


module.exports = {
  entry: {
    main: './src/index.js',
  },
  devtool: 'source-map',                              // This was stupidly set before to inline-source-map, now source-map which is
                                                      // much wiser. See this for more info about this config directive:
                                                      // https://webpack.js.org/configuration/devtool/#components/sidebar/sidebar.jsx
  devServer: {                                        // Also refer to plugins below, as contentBase is filled using HtmlWebPackPlugin
    contentBase: path.join(__dirname, 'examples'),    // In addition to the compiled entry point, make WDS serve files in ./examples
    hot: true,                                        // Enable hot reloading
    port: 8080,                                       // Run WDS (webpack-dev-server) on port 8080
    overlay: true,                                    // Display errors an overlay
    host: '0.0.0.0',
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),

    // Copy every single file in the ./examples folder into the dist/examples/ folder. Used with the devServer above
    ...fs.readdirSync(path.join(__dirname, 'examples')).map(exampleHtmlFile => new HtmlWebpackPlugin({
      filename: exampleHtmlFile,
      template: path.join(__dirname, `examples/${exampleHtmlFile}`),
      inject: 'head'
    })),

    new webpack.HotModuleReplacementPlugin(),
    new CompressionPlugin({
      test: /\.(js|css)$/,
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      deleteOriginalAssets: false
    }),
  ],
  module: {
    rules: [
      /*{
        test: /index.js$/,
        include: [
          path.resolve(__dirname, "node_modules", "d3-tip")
        ],
        use: 'imports-loader?this=>window'
      },*/
      {
        test: /datatables\.net.*/,
        use: 'imports-loader?define=>false'
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test:/\.(s*)css$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 8000, // Convert images < 8kb to base64 strings
            name: 'images/[hash]-[name].[ext]'
          }
        }]
      }
    ],
    noParse: /(mapbox-gl)\.js$/,            // Added to prevent some kind of corruption of mapbox-gl, see this link for details
                                            // https://github.com/mapbox/mapbox-gl-js/issues/4359#issuecomment-288001933
  },
  resolve: {
    extensions: ['*', '.js']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[hash].js',           //
    libraryTarget: 'umd',                   // Tells webpack to use UMD for compilation. This library is importable anywhere as result.
    library: 'NrcStatWidgetLibrary',        // Since wlib is a library project, we want everythingboiled down to this single variable
                                            // Essentials re. the last two directives: https://webpack.js.org/guides/author-libraries/
  },
  node: {
    fs: 'empty' // An error appeared solved by adding this. Details: https://github.com/webpack-contrib/css-loader/issues/447
  }
}
