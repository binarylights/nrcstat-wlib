
// Binary Brilliant Digital Lighthouse

const $ = require('jquery')
import mapboxgl from 'mapbox-gl'
import "./styles.scss"
import { includes, find, map as _map } from 'lodash'
import { formatDataNumber, formatDataPercentage, formatNumber, isMobileDevice } from '../../../old/widgetHelpers'
import norwegianCountryNames from '../../../old/assets/countryCodeNameMapNorwegian.json'
import countryLinks from './country-links.json'
import facebookIcon from './facebook.png'
import shareIcon from './share.png'
import twitterIcon from './twitter.png'
import linkedinIcon from './linkedin.png'
import closeButton from './close.png'
import middleResolutionCountriesGeoJson from './ne_110m_admin_0_countries.json'
import gazaGeoJson from './gaza.json'

middleResolutionCountriesGeoJson.features.push(gazaGeoJson.features[ 0 ])

const API_URL = "https://api.nrcdata.no/api/datas"
const QUERY = { where: { year: 2017 } }
let countryStatsCache = null
let isFullScreen
const nrcCountryIso2 = [ "AF", "CD", "CF", "CM", "CO", "DJ", "EC", "ER", "ET", "GR", "HN", "IQ", "IR", "JO", "KE", "LB", "ML", "MM", "NG", "PA", "PS", "SO", "SS", "SY", "TZ", "UA", "UG", "VE", "YE" ]
const blueCountryIso2 = [ "AT", "AU", "AZ", "BD", "BI", "CA", "CG", "CN", "DE", "EG", "FR", "GM", "IN", "IT", "MX", "NE", "NO", "PH", "PK", "RW", "SD", "SE", "SV", "TD", "TR", "US", "VN" ]
const toggleFullScreenAnimationDuration = 300

//made popup global variable to be able to access it from setPassiveMode (needs to be removed before deactivating the map) - not sure it's a best solution??
var countryInfo__mapboxPopup
const initialZoom = isMobileDevice() ? 2 : 2;
const initialCenter = isMobileDevice() ? [18.596136, 47.678121] : [ 23.639724, -2.799158 ]
var mapNavigationControl;
var mobileLegendActive = false;
var mobileShareMenuActive = false;


let beforeFullScreenCssProps = {}
let beforeFullScreenBodyOverflowProp;
let beforeFullScreenNrcPageHeaderZIndex;

function loadStats() {
  const url = `${API_URL}?filter=${encodeURIComponent(JSON.stringify(QUERY))}`
  $.getJSON(url)
      .then(function (data) {
        countryStatsCache = data
      })
      .catch(function (err) {
        console.log("error occurred during loading country stats data from loopback:")
        console.log(err)
      })
}

function getCountryStats(countryIso2Code) {
  if (typeof countryStatsCache) {
    return countryStatsCache.filter(c => c.countryCode === countryIso2Code)
  } else {
    return null
  }
}

function getCountryStat(countryIso2Code, dataPoint) {
  const stats = getCountryStats(countryIso2Code)
  if (!stats) return null
  const data = stats.filter(d => d.dataPoint === dataPoint)
  if (data && data.length > 0) return data[ 0 ]
  return null
}


function drawWidgetMainmap(widgetObject, widgetData, targetSelector) {
  $(targetSelector).empty();
  $(targetSelector).addClass("nrcstat-static-mainmap");
  $(targetSelector).css('position', 'relative')
  $(targetSelector).css('overflow', 'hidden')

  isFullScreen = false

  $(targetSelector).append(`
    <button class="map-open-button" type="button">Utforsk kart</button>
    <button class="map-close-button" style="display: none" type="button">Avslutt utforskning</button> 
    <div class="map-share"></div>
    <div id="mainmap-goes-here" style="width: 100%; height: 100%;"><div class="overlay"></div><div class="legend"></div></div>
  `);

  if (isMobileDevice()) {
    $(targetSelector).find("#mainmap-goes-here").css({
      position: 'absolute',
      height: window.innerHeight,
      width: window.innerWidth
    })
  }

  //highlight open button when clicked outside of it
  var animationEvent = 'webkitAnimationEnd oanimationend msAnimationEnd animationend';
  $(targetSelector).find(".overlay").mousedown(function () {
      $(targetSelector).find(".map-open-button").addClass('highlight-map-open-button');
      $(targetSelector).find(".map-open-button").one(animationEvent, function(event) {
          $(this).removeClass('highlight-map-open-button')
      });
  });

  $(targetSelector).find(".map-open-button").click(function () {
      setActiveMode(targetSelector, map);
  });

  $(targetSelector).find(".map-close-button").click(function () {
    setPassiveMode(targetSelector, map);
  });

  addLegend(targetSelector);

  addShareMenu(targetSelector);

  loadStats()


  mapboxgl.accessToken = 'pk.eyJ1IjoibnJjbWFwcyIsImEiOiJjaW5hNTM4MXMwMDB4d2tseWZhbmFxdWphIn0._w6LWU9OWnXak36BkzopcQ';
  var map = new mapboxgl.Map({
    container: "mainmap-goes-here",
    style: 'mapbox://styles/nrcmaps/cji8nwz770e7x2slhf6n6lshg',
    center: initialCenter,
    minZoom: 2,
    maxZoom: 5,
  });
  map.setZoom(initialZoom);

  //disable map rotation
  map.dragRotate.disable();
  map.touchZoomRotate.disableRotation();

  //change position of mapbox logo
  $(targetSelector).find(".mapboxgl-ctrl-bottom-left").removeClass("mapboxgl-ctrl-bottom-left").addClass("mapboxgl-ctrl-bottom-right");

  map.on('load', function () {

    map.addSource("countries", {
      "type": "geojson",
      "data": middleResolutionCountriesGeoJson
    });

    const sharedLayerProperties = {
      "type": "fill",
      "source": "countries",
      "paint": {
        "fill-opacity": 1
      },
      "filter": [ "==", "iso_a2", "" ]
    }

    map.addLayer(Object.assign(sharedLayerProperties, {
      "id": "countries-highlighted",
      "paint": { "fill-color": "#efeeec", },
    }));
    map.addLayer(Object.assign(sharedLayerProperties, {
      "id": "countries-highlighted-nrc",
      "paint": { "fill-color": "#ff7700", },
    }));
    map.addLayer(Object.assign(sharedLayerProperties, {
      "id": "countries-highlighted-blue",
      "paint": { "fill-color": "#d4d4d4", },
    }));

    function countryMouseMoveOverHandler(e) {
      const hoverCountryIso2 = e.features[ 0 ].properties.iso_a2;

      if (countryInfo__hasData(hoverCountryIso2) || e.features[ 0 ].properties.name == "Kosovo") {
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "default");
      }

      map.setFilter('countries-highlighted-nrc', [ '==', 'iso_a2', _.includes(nrcCountryIso2, hoverCountryIso2) ? hoverCountryIso2 : '' ]);
      map.setFilter('countries-highlighted-blue', [ '==', 'iso_a2', _.includes(blueCountryIso2, hoverCountryIso2) ? hoverCountryIso2 : '' ]);
      const nrcAndBlueIso2 = nrcCountryIso2.concat(blueCountryIso2)
      map.setFilter('countries-highlighted', [ '==', 'iso_a2', !_.includes(nrcAndBlueIso2) ? hoverCountryIso2 : '' ]);
    }

    //disable hover on mobile device otherwise country stays highlighted once popover is closed
    if(!isMobileDevice()){
      map.on("mousemove", "countries", countryMouseMoveOverHandler);
      map.on("mouseover", "countries", countryMouseMoveOverHandler)
    }

    map.on("click", "countries", function (event) {

      var selectedCountry = event.features[ 0 ].properties;

      if(mobileLegendActive || mobileShareMenuActive){
        $(targetSelector).find(".legend-container").css("display", "none");
        $(targetSelector).find("#legend-button").removeClass('legend-button-closed legend-button-open').addClass("legend-button-closed");
        setLegendState(targetSelector);
        $(targetSelector).find(".share-menu-container").css("display", "none");
        setShareMenuState(targetSelector);

      }  else{
        //insert Kosovo country code (has "name" but no "iso_a2" in natural earth data)
        if (selectedCountry.name == "Kosovo") {
          selectedCountry.iso_a2 = "KO";
        }

        //countries without iso2 code in naturalearth data have value -99 instead
        if (countryInfo__hasData(selectedCountry.iso_a2)) {
          if (isMobileDevice() && selectedCountry.iso_a2 != -99) {
            countryInfo__showPopover(targetSelector, event);
          } else if (selectedCountry.iso_a2 != -99) {
            countryInfo__showPopup(event, map);
          }
        }

      }
    });

    //event listener for closing legend and share menu by clicking on the non-country (e.g. sea)
    map.on("click", function (event){

      if(mobileLegendActive || mobileShareMenuActive){
        $(targetSelector).find(".legend-container").css("display", "none");
        $(targetSelector).find("#legend-button").removeClass('legend-button-closed legend-button-open').addClass("legend-button-closed");
        setLegendState(targetSelector);
        $(targetSelector).find(".share-menu-container").css("display", "none");
        setShareMenuState(targetSelector);
      }

    });


    map.on("mouseleave", "countries-highlighted", function () {
      $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "grab");
      $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-webkit-grab");
      $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-moz-grab");

      map.setFilter("countries-highlighted", [ "==", "iso_a2", "" ]);
      map.setFilter("countries-highlighted-blue", [ "==", "iso_a2", "" ]);
      map.setFilter("countries-highlighted-nrc", [ "==", "iso_a2", "" ]);
    });

    map.on("dragstart", function () {
      if ($(targetSelector).find(".mapboxgl-canvas-container").css("cursor") != "default") {
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "grabbing");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-webkit-grabbing");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-moz-grabbing");
      }
    });

    map.on("dragend", function (event) {
      if ($(targetSelector).find(".mapboxgl-canvas-container").css("cursor") != "default") {
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "grab");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-webkit-grab");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-moz-grab");
      }
    });

  });


}

//#region Country info (popover for mobile, popup for tablet/desktop)

function countryInfo__showPopover(targetSelector, event) {

  var selectedCountryIso2 = event.features[ 0 ].properties.iso_a2;
  const norwegianCountryName = norwegianCountryNames[ selectedCountryIso2 ]
  var fullCountryName = '<h1>' + norwegianCountryName + '</h1>';

  const statsTable = countryInfo__statsTable(selectedCountryIso2)

  const countryUrl = countryLinks[ selectedCountryIso2 ]
  const countryLink = countryUrl ?
      `<p class="country-link"><a href="https://www.flyktninghjelpen.no/${countryUrl}" target="_blank">Les mer om ${norwegianCountryName} her</a></p>` :
      ''

  //close menu when popover opens

  $(targetSelector).find("#legend-button").attr('class', 'legend-button-closed');
  $(targetSelector).find(".legend-container").css("display", "none");
  setLegendState(targetSelector);
  $(targetSelector).find(".share-menu-container").css("display", "none");
  setShareMenuState(targetSelector);

  const popupHtml =
    `
      <div class="nrcstat-map-country-info-popover-wrapper">
        <div class="popover-top-ribbon"></div>
        <span class="close-popover disable-selection"><img class="close-popup-img" src=" "></span>
        
        <div class="country-statistics">
             <p class="title">${norwegianCountryName}</p>
             <p class="statistics">${statsTable}</p>
             
             ${countryLink}
        </div>
      </div>
    `


  $(targetSelector).append(popupHtml)
  $(".close-popup-img").attr("src", closeButton);
  $('.nrcstat-map-country-info-popover-wrapper .close-popover').click(() => closePopover());
}

function closePopover() {

    $('.nrcstat-map-country-info-popover-wrapper').remove();

}

function countryInfo__showPopup(event, map) {
  var selectedCountryIso2 = event.features[ 0 ].properties.iso_a2;
  const norwegianCountryName = norwegianCountryNames[ selectedCountryIso2 ]
  var fullCountryName = '<h1>' + norwegianCountryName + '</h1>';
  var loader = '<div class="loader"></div>';

  countryInfo__mapboxPopup = new mapboxgl
      .Popup({
        closeButton: true,
        closeOnClick: true,
      })
      .setLngLat([ event.lngLat.lng, event.lngLat.lat ])
      .setHTML('<div class="popup-container">' + fullCountryName + loader + '</div>')
      .addTo(map);

  const statsTable = countryInfo__statsTable(selectedCountryIso2)

  const countryUrl = countryLinks[ selectedCountryIso2 ]
  const countryLink = countryUrl ?
      `<p class="country-link"><a href="https://www.flyktninghjelpen.no/${countryUrl}" target="_blank">Les mer om ${norwegianCountryName} her</a></p>` :
      ''

  countryInfo__mapboxPopup.setHTML('<div class="popup-container">' + fullCountryName + statsTable + countryLink + '</div>');
}



function countryInfo__statsTable(iso2) {
  const countryStats = getCountryStats(iso2);

  let dataPoints = [
    { dataPointKey: "totalRefugeesFromX", dataPointName: "Totalt antall som har flyktet fra" },
    { dataPointKey: "refugeesInXFromOtherCountriesInYear", dataPointName: "Totalt antall som har flyktet til" },
    { dataPointKey: "idpsInXInYear", dataPointName: "Totalt antall internt fordrevne" },
    { dataPointKey: "newRefugeesFromXInYear", dataPointName: "Nye flyktninger fra landet i 2017" },
    { dataPointKey: "newRefugeesInXFromOtherCountriesInYear", dataPointName: "Nye flyktninger til landet i 2017" },
    { dataPointKey: "newIdpsInXInYear", dataPointName: "Nye internt fordrevene i 2017" },
    { dataPointKey: "voluntaryReturnsToXInYear", dataPointName: "Frivillige tilbakevendinger til i 2017" },
    { dataPointKey: "asylumSeekersFromXToNorwayInYear", dataPointName: "Asylsøkere til Norge i 2017" },
    { dataPointKey: "population", dataPointName: "Folketall (millioner)" },
    { dataPointKey: "percentageWomenFleeingToCountry", dataPointName: "Andel kvinner blant flyktningene i landet" },
    { dataPointKey: "percentageChildrenFleeingToCountry", dataPointName: "Andel barn blant flyktningene i landet" },
  ]
  dataPoints = dataPoints.map(dp => {
    let dataPointValue = getCountryStat(iso2, dp.dataPointKey).data

    if (includes([ 'percentageWomenFleeingToCountry', 'percentageChildrenFleeingToCountry' ], dp.dataPointKey))
      dataPointValue = formatDataPercentage(dataPointValue, "nb_NO")
    else
      dataPointValue =  formatDataNumber(dataPointValue, "nb_NO", true);

    Object.assign(dp, { dataPointValue })
    return dp
  })

  const descriptionData = dataPoints.map(
      dp => `<tr><td class="statistics-label">${dp.dataPointName}:</td><td align="right" class="statistics-number">${dp.dataPointValue}</td></tr>`
  ).join("\n")

  const table = `<table>${descriptionData}</table>`

  return table
}

function countryInfo__hasData(iso2) {
  return !!_.find(countryStatsCache, c => c.countryCode === iso2);
}

//#endregion

//#region Map legend


function animateFullScreen(elm, map, callbackCb) {

  const target = $(elm)

  if (!isFullScreen) {

    beforeFullScreenNrcPageHeaderZIndex = $("header.page-header").css("z-index")
    $("header.page-header").css("z-index", 0)

    beforeFullScreenBodyOverflowProp = $('body').css('overflow')
    $('body').css('overflow', 'hidden')

    const windowWidth = window.innerWidth
    const windowHeight = window.innerHeight

    const { top: topOffset, left: leftOffset } = target.offset()
    const scrollTop = $(document).scrollTop()

    let nonComputedStyles = target.attr("style").split(";")
    nonComputedStyles = nonComputedStyles.map(style => style.split(":"));
    const nonComputedStyleByName = (name) => {
      return undefined;
      const style = _.find(nonComputedStyles, style => style[0] === name)
      if (style) return style[1]
      else return undefined;
    }

    beforeFullScreenCssProps = {
      top: nonComputedStyleByName("top") || target.css('top'),
      left: nonComputedStyleByName("left") || target.css('left'),
      width: nonComputedStyleByName("width") || target.css('width'),
      height: nonComputedStyleByName("height") || target.css('height'),
    }
    const newProps = {
      top: `${-Math.floor(topOffset-scrollTop)}px`,
      left: `-${leftOffset}px`,
      width: `${windowWidth}px`,
      height: `${windowHeight}px`,
    }
    target.animate(newProps, toggleFullScreenAnimationDuration, () => {
      isFullScreen = true
      callbackCb()
    })

  } else {

    $('body').css('overflow', beforeFullScreenBodyOverflowProp)

    $("header.page-header").css("z-index", beforeFullScreenNrcPageHeaderZIndex)
    target.animate(beforeFullScreenCssProps, toggleFullScreenAnimationDuration, () => {
      isFullScreen = false
      //const cssAttr = _.map(beforeFullScreenCssProps, (v, k) => `${k}: ${v}`).join(";") + ";"
      //target.attr('style', cssAttr)
      callbackCb();
    })

  }
}

function setActiveMode(targetSelector, map) {
    $(targetSelector).find(".overlay").addClass('disappear');
    $(targetSelector).find(".map-open-button").css("display", "none");
    $(targetSelector).find(".map-open-button").removeClass('highlight-map-open-button');


  const activateMap = () => {


        var animationEvent = 'webkitAnimationEnd oanimationend msAnimationEnd animationend';
        $(targetSelector).find(".overlay").one(animationEvent, function (event) {
            $(targetSelector).find(".overlay").removeClass('disappear');
            $(targetSelector).find(".overlay").css("display", "none");

            mapNavigationControl = new mapboxgl.NavigationControl({
                showCompass: false
            })

            map.addControl(mapNavigationControl, 'bottom-right');

            $(targetSelector).find(".map-close-button").css("display", "block");

        });
    }

    if(isMobileDevice()){
        animateFullScreen(targetSelector, map, activateMap)
    }
    else{
        activateMap()
    }
}

function setPassiveMode(targetSelector, map) {
    //check if popup exist otherwise remove function returns error
    if(countryInfo__mapboxPopup != undefined){
      countryInfo__mapboxPopup.remove();
    }

  $(targetSelector).find(".map-close-button").css("display", "none");
  $(targetSelector).find(".share-menu-container").css("display", "none");
  setShareMenuState(targetSelector);
  $(targetSelector).find(".legend-container").css("display", "none");
  $(targetSelector).find("#legend-button").removeClass('legend-button-closed legend-button-open').addClass("legend-button-closed");
  setLegendState(targetSelector);
  $(targetSelector).find(".overlay").css("display", "block");
  $(targetSelector).find(".overlay").removeClass('disappear').addClass("appear");

    const deactivateMap = () => {
      var animationEvent = 'webkitAnimationEnd oanimationend msAnimationEnd animationend';
      $(targetSelector).find(".overlay").one(animationEvent, function(event) {
        map.flyTo({center: initialCenter, zoom: initialZoom});
        $(targetSelector).find(".overlay").removeClass('appear');
        $(targetSelector).find(".map-open-button").css("display", "block");

        map.removeControl(mapNavigationControl);
      });
    }

    if(isMobileDevice()){
      animateFullScreen(targetSelector, map, deactivateMap)
    }
    else{
      deactivateMap()
    }

}


function addLegend(targetSelector) {
  const legend = $(targetSelector).find('.legend')

  if (isMobileDevice()) {
    addLegendMobile(legend);
  } else {
    addLegendTabletDesktop(legend);
  }

  $(targetSelector).find(".legend-button-container").click(function () {
    $(targetSelector).find(".share-menu-container").css("display", "none");
    setShareMenuState(targetSelector);
    $(targetSelector).find(".legend-container").toggle();
    $(targetSelector).find("#legend-button").toggleClass('legend-button-closed');
    $(targetSelector).find("#legend-button").toggleClass('legend-button-open');
    setLegendState(targetSelector)
  });
}

const fullLegend = `
      <table>
        <tr>
            <td><span class="orange-dot"></span></td>
            <td class="legend-text">Land der Flyktninghjelpen jobber</td>
        </tr>
        <tr>
            <td><span class="gray-dot"></span></td>
            <td class="legend-text">Andre land hvor mange har flyktet eller som har tatt i mot flyktninger</td>
        </tr>
      </table>
      
      <p><span class="source"> Kilde: UNHCR, IDMC </span></p>
    `;

function addLegendMobile(legend) {
  $(legend).append(
      `<div class="legend-button-container">
            <a class="disable-selection">
              <div class="legend-label">&#9432;</div>
              <div id="legend-button"  class="legend-button-closed">
                <span style="color: #FF7602;">&gt;</span><span style="color: #d4d4d4;">&gt;</span>
              </div>
            </a>
          </div>
          <div id="legend-container" class="legend-container" style="display: none;"></div>
          `
  );
  $(legend).find(".legend-container").append($(fullLegend))
}

function addLegendTabletDesktop(legend) {
  $(legend).append(`<div id="legend-container" class="legend-container-desktop"></div>`);
  $(legend).find(".legend-container-desktop").append($(fullLegend))
}


function setLegendState (targetSelector) {
  if($(targetSelector).find("#legend-button").hasClass('legend-button-closed'))
    mobileLegendActive = false;
  else if($(targetSelector).find("#legend-button").hasClass('legend-button-open'))
    mobileLegendActive = true;
}

function setShareMenuState (targetSelector) {
  if($(targetSelector).find(".share-menu-container").css('display') != 'none')
    mobileShareMenuActive = true;

  else
    mobileShareMenuActive = false;

}

function addShareMenu(targetSelector) {
    const shareMenu = $(targetSelector).find('.map-share');

    shareMenu.append(`
        <div class="share-button-container">
            <div class="map-share-button"><img class="share-icon"></div>
        </div>
        <div class="share-menu-container"></div>
    `);

  const fbUrl = facebookHandler(targetSelector)
  const liUrl = linkedinHandler(targetSelector)
  const twUrl = twitterHandler(targetSelector)

  const fullShareMenu = `      
      <table>
        <tr>
            <td class="handler facebook-link share-logo disable-selection"><a href="${fbUrl}" target="_blank"><img class="facebook-icon" src="" /></a></td>
            <td class="handler facebook-link disable-selection"><a href="${fbUrl}" target="_blank"> Facebook</a></td>
        </tr>
        <tr>
            <td class="handler linkedin-link share-logo disable-selection"><a href="${liUrl}" target="_blank"><img class="linkedin-icon" src="" /></a></td>
            <td class="handler linkedin-link disable-selection"><a href="${liUrl}" target="_blank"> Linkedin</a></td>
        </tr>
        <tr>
            <td class="handler twitter-link share-logo disable-selection"><a href="${twUrl}" target="_blank"><img class="twitter-icon" src="" /></a></td>
            <td class="handler twitter-link disable-selection"><a href="${twUrl}" target="_blank"> Twitter</a></td>
        </tr>
      </table>
      
    `;

    shareMenu.find(".share-menu-container").append($(fullShareMenu));

    $(".facebook-icon").attr("src",facebookIcon);
    $(".linkedin-icon").attr("src",linkedinIcon);
    $(".twitter-icon").attr("src",twitterIcon);
    $(".share-icon").attr("src",shareIcon);

    shareMenu.find(".share-menu-container").css("display", "none");

    shareMenu.find(".share-button-container").mousedown(function () {
      shareMenu.find(".share-menu-container").toggle();
      $(targetSelector).find(".legend-container").css("display", "none");
      $(targetSelector).find("#legend-button").removeClass('legend-button-closed legend-button-open').addClass("legend-button-closed");
      setShareMenuState(targetSelector);
      setLegendState(targetSelector);

      if(countryInfo__mapboxPopup != undefined){
        countryInfo__mapboxPopup.remove();
      }
    });

    shareMenu.find(".facebook-link").on("click", () => facebookHandler(targetSelector));
    shareMenu.find(".linkedin-link").on("click", () => linkedinHandler(targetSelector));
    shareMenu.find(".twitter-link").on("click", () => twitterHandler(targetSelector));


}

function facebookHandler (targetElementAttrId) {
  var originalWidgetUrlToShare = window.location.href.split("#")[ 0 ]
  if (targetElementAttrId) originalWidgetUrlToShare += targetElementAttrId
  var href = 'https://api.nrcdata.no/api/widgets/mainmap-2018-0.2/render/false?orgWUrl=' + encodeURIComponent(originalWidgetUrlToShare)
  var url = "https://www.facebook.com/dialog/share?" +
      "app_id=1769614713251596" +
      "&display=popup" +
      "&href=" + encodeURIComponent(href)

  return url;
}

function linkedinHandler (targetElementAttrId) {
  var originalWidgetUrlToShare = window.location.href.split("#")[0]
  if (targetElementAttrId) originalWidgetUrlToShare += targetElementAttrId
  var href = 'https://api.nrcdata.no/api/widgets/mainmap-2018-0.2/render/false?orgWUrl=' + encodeURIComponent(originalWidgetUrlToShare)
  var url = "http://www.linkedin.com/shareArticle?" +
    'url=' + href +
    '&mini=true'
  return url;
}

function twitterHandler (targetElementAttrId) {
  var originalWidgetUrlToShare = window.location.href.split("#")[0]
  if (targetElementAttrId) originalWidgetUrlToShare += targetElementAttrId
  var href = 'https://api.nrcdata.no/api/widgets/mainmap-2018-0.2/render/false?orgWUrl=' + encodeURIComponent(originalWidgetUrlToShare)
  var url = "https://twitter.com/intent/tweet?" +
    'text=' + href
  return url;
}

//#endregion

export { drawWidgetMainmap }
