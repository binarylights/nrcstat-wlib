const $ = require('jquery')
import mapboxgl from 'mapbox-gl'
import "./styles.scss"
import { includes } from 'lodash'
import { formatNumber, isMobileDevice } from '../../../old/widgetHelpers'
import norwegianCountryNames from '../../../old/assets/countryCodeNameMapNorwegian.json'
import countryLinks from './country-links.json'
import middleResolutionCountriesGeoJson from './ne_110m_admin_0_countries.json'
import gazaGeoJson from './gaza.json'
import infoIcon from './info-icon.png'

middleResolutionCountriesGeoJson.features.push(gazaGeoJson.features[ 0 ])

const API_URL = "https://api.nrcdata.no/api/datas"
const QUERY = { where: { year: 2016 } }
let countryStatsCache = null

function loadStats() {
  const url = `${API_URL}?filter=${encodeURIComponent(JSON.stringify(QUERY))}`
  $.getJSON(url)
      .then(function (data) {
        countryStatsCache = data
      })
      .catch(function (err) {
        console.log("error occurred during loading country stats data from loopback:")
        console.log(err)
      })
}

function getCountryStats(countryIso2Code) {
  if (typeof countryStatsCache) {
    return countryStatsCache.filter(c => c.countryCode === countryIso2Code)
  } else {
    return null
  }
}

function getCountryStat(countryIso2Code, dataPoint) {
  const stats = getCountryStats(countryIso2Code)
  if (!stats) return null
  const data = stats.filter(d => d.dataPoint === dataPoint)
  if (data && data.length > 0) return data[ 0 ]
  return null
}


function drawWidgetMainmap(widgetObject, widgetData, targetSelector) {

  $(targetSelector).empty()
  $(targetSelector).addClass("nrcstat-static-mainmap")

  $(targetSelector).append(`<div id="mainmap-goes-here" style="width: 100%; height: 100%;"><div class="legend"></div></div>`);

  addLegend(targetSelector);

  loadStats()

  mapboxgl.accessToken = 'pk.eyJ1IjoibnJjbWFwcyIsImEiOiJjaW5hNTM4MXMwMDB4d2tseWZhbmFxdWphIn0._w6LWU9OWnXak36BkzopcQ';
  var map = new mapboxgl.Map({
    container: "mainmap-goes-here",
    style: 'mapbox://styles/nrcmaps/cji8nwz770e7x2slhf6n6lshg',
    center: [ 23.639724, -2.799158 ],
    minZoom: 1,
    maxZoom: 5,
  });

  if (isMobileDevice()) {
    map.setZoom(1.7);
  }
  else map.setZoom(2);


  map.on('load', function () {

    var nav = new mapboxgl.NavigationControl();
    map.addControl(nav, 'bottom-right');

    $(targetSelector).find(".menu-button-container").click(function () {
      $(targetSelector).find(".legend-container").toggle();
      $(targetSelector).find("#menu-button").toggleClass('menu-button-closed');
      $(targetSelector).find("#menu-button").toggleClass('menu-button-open');
    });

    map.addSource("countries", {
      "type": "geojson",
      "data": middleResolutionCountriesGeoJson
    });


    const sharedLayerProperties = {
      "type": "fill",
      "source": "countries",
      "paint": {
        "fill-opacity": 1
      },
      "filter": [ "==", "iso_a2", "" ]
    }

    map.addLayer(Object.assign(sharedLayerProperties, {
      "id": "countries-highlighted",
      "paint": { "fill-color": "#efeeec", },
    }));
    map.addLayer(Object.assign(sharedLayerProperties, {
      "id": "countries-highlighted-nrc",
      "paint": { "fill-color": "#ff7700", },
    }));
    map.addLayer(Object.assign(sharedLayerProperties, {
      "id": "countries-highlighted-blue",
      "paint": { "fill-color": "#d4d4d4", },
    }));

    function countryMouseMoveOverHandler(e) {
      const hoverCountryIso2 = e.features[ 0 ].properties.iso_a2;

      if (countryInfo__hasData(hoverCountryIso2) || e.features[ 0 ].properties.name == "Kosovo") {
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "default");
      }

      const nrcCountryIso2 = [ "AF", "CD", "CF", "CM", "CO", "DJ", "EC", "ER", "ET", "GR", "HN", "IQ", "IR", "JO", "KE", "LB", "ML", "MM", "NG", "PA", "PS", "SO", "SS", "SY", "TZ", "UA", "UG", "VE", "YE" ]
      const blueCountryIso2 = [ "AT", "AU", "AZ", "BD", "BI", "CA", "CG", "CN", "DE", "EG", "FR", "GM", "IN", "IT", "MX", "NE", "NO", "PH", "PK", "RW", "SD", "SE", "SV", "TD", "TR", "US", "VN" ]


      map.setFilter('countries-highlighted-nrc', [ '==', 'iso_a2', _.includes(nrcCountryIso2, hoverCountryIso2) ? hoverCountryIso2 : '' ]);
      map.setFilter('countries-highlighted-blue', [ '==', 'iso_a2', _.includes(blueCountryIso2, hoverCountryIso2) ? hoverCountryIso2 : '' ]);
      const nrcAndBlueIso2 = nrcCountryIso2.concat(blueCountryIso2)
      map.setFilter('countries-highlighted', [ '==', 'iso_a2', !_.includes(nrcAndBlueIso2) ? hoverCountryIso2 : '' ]);
    }

    map.on("mousemove", "countries", countryMouseMoveOverHandler);
    map.on("mouseover", "countries", countryMouseMoveOverHandler)

    map.on("click", "countries", function (event) {

      var selectedCountry = event.features[ 0 ].properties;

      if (selectedCountry.name == "Kosovo") {
        selectedCountry.iso_a2 = "KO";
      }

      if (countryInfo__hasData(selectedCountry.iso_a2)) {
        if (isMobileDevice() && selectedCountry.iso_a2 != -99) {
          countryInfo__showPopover(targetSelector, event);
        } else if (selectedCountry.iso_a2 != -99) {
          countryInfo__showPopup(event, map);
        }
      }

    });

    map.on("mouseleave", "countries-highlighted", function () {
      $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "grab");
      $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-webkit-grab");
      $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-moz-grab");

      map.setFilter("countries-highlighted", [ "==", "iso_a2", "" ]);
      map.setFilter("countries-highlighted-blue", [ "==", "iso_a2", "" ]);
      map.setFilter("countries-highlighted-nrc", [ "==", "iso_a2", "" ]);
    });

    map.on("dragstart", function () {
      if ($(targetSelector).find(".mapboxgl-canvas-container").css("cursor") != "default") {
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "grabbing");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-webkit-grabbing");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-moz-grabbing");
      }
    });

    map.on("dragend", function (event) {
      if ($(targetSelector).find(".mapboxgl-canvas-container").css("cursor") != "default") {
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "grab");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-webkit-grab");
        $(targetSelector).find(".mapboxgl-canvas-container").css("cursor", "-moz-grab");
      }
    });

  });


}

//#region Country info (popover for mobile, popup for tablet/desktop)

function countryInfo__showPopover(targetSelector, event) {

  var selectedCountryIso2 = event.features[ 0 ].properties.iso_a2;
  const norwegianCountryName = norwegianCountryNames[ selectedCountryIso2 ]
  var fullCountryName = '<h1>' + norwegianCountryName + '</h1>';

  const statsTable = countryInfo__statsTable(selectedCountryIso2)

  const countryUrl = countryLinks[ selectedCountryIso2 ]
  const countryLink = countryUrl ?
      `<p class="country-link"><a href="https://www.flyktninghjelpen.no/${countryUrl}" target="_blank">Les mer om ${norwegianCountryName} her</a></p>` :
      ''

  //close menu when popover opens

  $(targetSelector).find("#menu-button").attr('class', 'menu-button-closed');
  $(targetSelector).find(".legend-container").css("display", "none");


  const popupHtml =
      `
<div class="nrcstat-map-country-info-popover-wrapper">
  <div class="popover-top-ribbon"></div>
  <span class="close-popover">X</span>
  
  <div class="country-statistics">
       <p class="title">${norwegianCountryName}</p>
       <p class="statistics">${statsTable}</p>
       
       ${countryLink}
  </div>
</div>
`

  $('body').append(popupHtml)
  $('.nrcstat-map-country-info-popover-wrapper .close-popover').click(() => $('.nrcstat-map-country-info-popover-wrapper').remove())
}

function countryInfo__showPopup(event, map) {
  var selectedCountryIso2 = event.features[ 0 ].properties.iso_a2;
  const norwegianCountryName = norwegianCountryNames[ selectedCountryIso2 ]
  var fullCountryName = '<h1>' + norwegianCountryName + '</h1>';
  var loader = '<div class="loader"></div>';

  var info = new mapboxgl
      .Popup({
        closeButton: false,
        closeOnClick: true,
      })
      .setLngLat([ event.lngLat.lng, event.lngLat.lat ])
      .setHTML('<div class="popup-container">' + fullCountryName + loader + '</div>')
      .addTo(map);

  const statsTable = countryInfo__statsTable(selectedCountryIso2)

  const countryUrl = countryLinks[ selectedCountryIso2 ]
  const countryLink = countryUrl ?
      `<p class="country-link"><a href="https://www.flyktninghjelpen.no/${countryUrl}" target="_blank">Les mer om ${norwegianCountryName} her</a></p>` :
      ''

  info.setHTML('<div class="popup-container">' + fullCountryName + statsTable + countryLink + '</div>');
}

function countryInfo__statsTable(iso2) {
  const countryStats = getCountryStats(iso2);

  let dataPoints = [
    { dataPointKey: "totalRefugeesFromX", dataPointName: "Totalt antall som har flyktet fra" },
    { dataPointKey: "refugeesInXFromOtherCountriesInYear", dataPointName: "Totalt antall som har flyktet til" },
    { dataPointKey: "idpsInXInYear", dataPointName: "Totalt antall internt fordrevne" },
    { dataPointKey: "newRefugeesFromXInYear", dataPointName: "Nye flyktninger fra landet i 2017" },
    { dataPointKey: "newRefugeesInXFromOtherCountriesInYear", dataPointName: "Nye flyktninger til landet i 2017" },
    { dataPointKey: "newIdpsInXInYear", dataPointName: "Nye internt fordrevene i 2017" },
    { dataPointKey: "voluntaryReturnsToXInYear", dataPointName: "Frivillige tilbakevendinger til i 2017" },
    { dataPointKey: "asylumSeekersFromXToNorwayInYear", dataPointName: "Asylsøkere til Norge i 2017" },
    { dataPointKey: "population", dataPointName: "Folketall (millioner)" },
    { dataPointKey: "percentageWomenFleeingToCountry", dataPointName: "Andel kvinner blant flyktningene i landet" },
    { dataPointKey: "percentageChildrenFleeingToCountry", dataPointName: "Andel barn blant flyktningene i landet" },
  ]
  dataPoints = dataPoints.map(dp => {
    let dataPointValue = getCountryStat(iso2, dp.dataPointKey)
    if (!dataPointValue.data) {
      dataPointValue = "N/A"
    } else {
      dataPointValue = dataPointValue.data
      if (includes([ 'percentageWomenFleeingToCountry', 'percentageChildrenFleeingToCountry' ], dp.dataPointKey)) dataPointValue = (dataPointValue * 100).toFixed(1) + "%"
      else dataPointValue = formatNumber(dataPointValue)
    }
    Object.assign(dp, { dataPointValue })
    return dp
  })

  const descriptionData = dataPoints.map(
      dp => `<tr><td>${dp.dataPointName}:</td><td align="right" class="statistics-number">${dp.dataPointValue}</td></tr>`
  ).join("\n")

  const table = `<table>${descriptionData}</table>`

  return table
}

function countryInfo__hasData(iso2) {
  return !!_.find(countryStatsCache, c => c.countryCode === iso2);
}

//#endregion

//#region Map legend

function addLegend(targetSelector) {
  const legend = $(targetSelector).find('.legend')

  if (isMobileDevice()) {
    addLegendMobile(legend);
  } else {
    addLegendTabletDesktop(legend);
  }
}

const fullLegend = `
      <table>
        <tr>
            <td><span class="orange-dot"></span></td>
            <td class="legend-text">Land der Flyktninghjelpen jobber</td>
        </tr>
        <tr>
            <td><span class="gray-dot"></span></td>
            <td class="legend-text">Andre land hvor mange har flyktet eller som har tatt i mot flyktninger</td>
        </tr>
      </table>
      
      <p><span class="source"> Kilde: UNHCR, IDMC </span></p>
    `;

function addLegendMobile(legend) {
  $(legend).append(
      `<div class="menu-button-container">
            <a class="disable-selection">
              <div class="legend-label">&#9432;</div>
              <div id="menu-button" class="menu-button-closed">
                <span style="color: #FF7602;">&gt;</span><span style="color: #d4d4d4;">&gt;</span>
              </div>
            </a>
          </div>
          <div class="legend-container" style="display: none;"></div>
          `
  );
  $(legend).find(".legend-container").append($(fullLegend))
}

function addLegendTabletDesktop(legend) {
  $(legend).append(`<div class="legend-container"></div>`);
  $(legend).find(".legend-container").append($(fullLegend))
}

//#endregion

export { drawWidgetMainmap }
