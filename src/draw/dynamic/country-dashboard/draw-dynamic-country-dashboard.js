// Binary Brilliant Digital Lighthouse

const $ = require('jquery');
import mapboxgl from 'mapbox-gl';
import './styles.scss';
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

import {
  includes,
  find,
  map as _map,
  map,
  groupBy,
  mapValues,
  keyBy,
  last,
  chain,
  clone,
  isNull,
} from 'lodash';
import {
  formatDataNumber,
  formatDataPercentage,
  formatNumber,
  isMobileDevice,
} from '../../../old/widgetHelpers';
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  LineSeries,
  CircularGridLines,
  MarkSeries,
  ArcSeries,
  VerticalGridLines,
  LabelSeries,
} from 'react-vis';
import 'react-vis/dist/style.css';
import norwegianCountryNames from '../../../old/assets/countryCodeNameMapNorwegian.json';
import horizontalBarBaseImg from './dashboard-horizontal-bar-base.png';
import middleResolutionCountriesGeoJson from '../../static/mainmap-v2/ne_110m_admin_0_countries.json';
import gazaGeoJson from '../../static/mainmap-v2/gaza.json';

middleResolutionCountriesGeoJson.features.push(gazaGeoJson.features[0]);

const req = require.context('../../../common/assets/flags', false);
// TOOD: use flow here instead, fp style, this below probably imports a lot of stuf
const flagImagesMap = chain(
  req.keys().map(file => ({
    file: req(file),
    countryCode: last(file.split('/'))
      .split('.')[0]
      .toUpperCase(),
  }))
)
  .keyBy('countryCode')
  .mapValues('file')
  .value();

import centroidsRaw from '../../static/global-displacement-radial-bar-chart/geo_entities_updated_manually';

import { API_URL } from '../../../old/baseConfig';

const YEAR_TO_SHOW_IN_RADIAL_BAR_CHART = 2018;

function HorizontalBar({ label, fraction, style }) {
  return (
    <table style={{ ...style, width: '100%' }}>
      <tbody>
        <tr>
          <td
            style={{
              width: '50%',
              verticalAlign: 'middle',
            }}
          >
            {label.toUpperCase()}
          </td>
          <td
            style={{
              textAlign: 'right',
              width: '3.5em',
              verticalAlign: 'middle',
              paddingRight: '0.4em',
            }}
            dangerouslySetInnerHTML={{
              __html: !isNull(fraction)
                ? formatDataPercentage(fraction, 'nb_NO').replace(' ', '&nbsp;')
                : '',
            }}
          />
          <td>
            <div
              style={{
                display: 'inline-block',
                width: '100%',
                height: '30px',
                backgroundColor: isNull(fraction) ? '' : 'lightgrey',
              }}
            >
              {!isNull(fraction) && (
                <div
                  style={{
                    display: 'inline-block',
                    width: `${fraction * 100}%`,
                    height: '30px',
                    backgroundImage: `url(${horizontalBarBaseImg})`,
                  }}
                />
              )}
              {isNull(fraction) && <React.Fragment>-</React.Fragment>}
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

function Dashboard({ data, countryCode, dataPointsToShow, onAfterRender }) {
  const [leftColSelectedOption, setLeftColSelectedOption] = useState('total');
  const [rightColSelectedOption, setRightColSelectedOption] = useState(
    String(YEAR_TO_SHOW_IN_RADIAL_BAR_CHART)
  );
  useEffect(() => {
    onAfterRender();
  });

  const COL_SELECT_OPTIONS = [
    {
      label: 'TOTAL',
      value: 'total',
    },
    {
      label: '2016',
      value: '2016',
    },
    {
      label: '2017',
      value: '2017',
    },
    {
      label: '2018',
      value: '2018',
    },
  ];
  const TABLE_ROWS = [
    {
      label: 'FLYKTNINGER FRA XXX',
      totalDataPoint: 'totalRefugeesFromX',
      newInYearXDataPoint: 'newRefugeesFromXInYear',
    },
    {
      label: 'FLYKTNINGER TIL XXX',
      totalDataPoint: 'refugeesInXFromOtherCountriesInYear',
      newInYearXDataPoint: 'newRefugeesInXFromOtherCountriesInYear',
    },
    {
      label: 'INTERNT FORDREVNE I XXX',
      totalDataPoint: 'idpsInXInYear',
      newInYearXDataPoint: 'newIdpsInXInYear',
    },
    {
      label: 'FRIVILLIGE TILBAKEVENDINGER TIL XXX',
      totalDataPoint: null,
      newInYearXDataPoint: 'voluntaryReturnsToXInYear',
    },
    {
      label: 'ASYLSØKERE TIL NORGE FRA XXX',
      totalDataPoint: null,
      newInYearXDataPoint: 'asylumSeekersFromXToNorwayInYear',
    },
  ];
  const DATA_POINT_POPULATION = 'population';
  const DATA_POINT_PERCENTAGE_CHILDREN_FLEEING_TO_COUNTRY =
    'percentageChildrenFleeingToCountry';
  const DATA_POINT_PERCENTAGE_WOMEN_FLEEING_TO_COUNTRY =
    'percentageWomenFleeingToCountry';

  const tableRows = TABLE_ROWS.filter(
    row =>
      dataPointsToShow.includes(row.totalDataPoint) ||
      dataPointsToShow.includes(row.newInYearXDataPoint)
  ).map(row => {
    const leftColStat = getCountryStat(
      data,
      countryCode,
      leftColSelectedOption === 'total'
        ? row.totalDataPoint
        : row.newInYearXDataPoint,
      leftColSelectedOption === 'total'
        ? YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
        : parseInt(leftColSelectedOption)
    );
    const rightColStat = getCountryStat(
      data,
      countryCode,
      rightColSelectedOption === 'total'
        ? row.totalDataPoint
        : row.newInYearXDataPoint,
      rightColSelectedOption === 'total'
        ? YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
        : parseInt(rightColSelectedOption)
    );

    return (
      <tr>
        <td>
          {row.label
            .replace('XXX', norwegianCountryNames[countryCode])
            .toUpperCase()}
        </td>
        <td className="data-cell">
          {formatDataNumber(
            leftColStat ? leftColStat.data : null,
            'nb_NO',
            true
          )}
        </td>
        <td className="data-cell">
          {formatDataNumber(
            rightColStat ? rightColStat.data : null,
            'nb_NO',
            true
          )}
        </td>
      </tr>
    );
  });

  const population = getCountryStat(
    data,
    countryCode,
    DATA_POINT_POPULATION,
    YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
  ).data;

  const dataPoint_percentageWomenFleeingToCountry = getCountryStat(
    data,
    countryCode,
    DATA_POINT_PERCENTAGE_WOMEN_FLEEING_TO_COUNTRY,
    YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
  );
  const dataPoint_percentageChildrenFleeingToCountry = getCountryStat(
    data,
    countryCode,
    DATA_POINT_PERCENTAGE_CHILDREN_FLEEING_TO_COUNTRY,
    YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
  );

  return (
    <React.Fragment>
      <img
        src={flagImagesMap[countryCode]}
        style={{ height: '2.4em', float: 'right', border: '1px solid #d4d4d4' }}
      />
      <p
        style={{
          margin: 0,
          padding: 0,
          color: '#ff7602',
          textTransform: 'uppercase',
          fontFamily: "'Roboto Condensed'",
          fontSize: '2em',
        }}
      >
        {norwegianCountryNames[countryCode]}
      </p>
      {dataPointsToShow.includes(DATA_POINT_POPULATION) && (
        <p
          style={{
            margin: '0.2em 0 0 0',
            padding: 0,
            color: '#666666',
            fontFamily: "'Roboto Condensed'",
            fontSize: '1em',
          }}
        >
          FOLKETALL: {population} millioner
        </p>
      )}
      {tableRows.length > 0 && (
        <table className="main-data-table" style={{ marginTop: '2.5em' }}>
          <tbody>
            <tr>
              <td />
              <td className="data-header-cell">
                <select
                  className="option-select"
                  onChange={e => setLeftColSelectedOption(e.target.value)}
                >
                  {COL_SELECT_OPTIONS.map(option => (
                    <option
                      value={option.value}
                      selected={option.value === leftColSelectedOption}
                    >
                      {option.label}
                    </option>
                  ))}
                </select>
              </td>
              <td className="data-header-cell">
                <select
                  className="option-select"
                  onChange={e => setRightColSelectedOption(e.target.value)}
                >
                  {COL_SELECT_OPTIONS.map(option => (
                    <option
                      key={option.value}
                      value={option.value}
                      selected={option.value === rightColSelectedOption}
                    >
                      {option.label}
                    </option>
                  ))}
                </select>
              </td>
            </tr>
            {tableRows}
          </tbody>
        </table>
      )}
      {dataPointsToShow.includes(
        DATA_POINT_PERCENTAGE_WOMEN_FLEEING_TO_COUNTRY
      ) && (
        <HorizontalBar
          label="Andel kvinner blant flyktningene i landet"
          fraction={
            dataPoint_percentageWomenFleeingToCountry
              ? dataPoint_percentageWomenFleeingToCountry.data
              : null
          }
          style={{ marginTop: '1em' }}
        />
      )}
      {dataPointsToShow.includes(
        DATA_POINT_PERCENTAGE_CHILDREN_FLEEING_TO_COUNTRY
      ) && (
        <HorizontalBar
          label="Andel barn blant flyktningene i landet"
          fraction={
            dataPoint_percentageChildrenFleeingToCountry
              ? dataPoint_percentageChildrenFleeingToCountry.data
              : null
          }
          style={{ marginTop: '1.3em' }}
        />
      )}
      <p className="footnote" style={{ marginBottom: '-0.5em' }}>
        Tallene gjelder ved utgangen til hvert kalenderår.
      </p>
      <p className="footnote" style={{ marginBottom: '5px' }}>
        Kilder: FNs høykommissær for flyktninger (UNHCR) og FNs
        hjelpeorganisasjon for Palestina-flyktninger (UNRWA).
      </p>
    </React.Fragment>
  );
}

function drawCountryDashboard(widgetObject, widgetData, targetSelector) {
  const widgetId = widgetObject.id;
  const { countryCode, year, dataPoints, showMap } = parseWidgetId(widgetId);

  const leonardoCentroid = getCountryCentroid(countryCode);
  const leonardoBoundingBox = leonardoCentroid.boundingbox;
  const [west, south, east, north] = leonardoBoundingBox;

  clearWidgetContainer();
  const { dataBlock, mapBlock } = drawWidgetContainer();

  configureMapboxAccessToken();
  const map = initializeMap(mapBlock, [0, 0]);
  const mapLoaded = () =>
    new Promise(resolve => map.on('load', () => resolve()));

  const yalla = () => new Promise(resolve => setTimeout(() => resolve(), 1000));

  Promise.all([fetchData(countryCode), mapLoaded(), yalla()]).then(([data]) => {
    drawDataBlock(dataBlock, data);
    drawMapBlock(
      mapBlock,
      data.filter(d => d.year === YEAR_TO_SHOW_IN_RADIAL_BAR_CHART)
    );
  });

  function clearWidgetContainer() {
    $(targetSelector).empty();
  }

  function drawWidgetContainer() {
    $(targetSelector).addClass('nrcstat-country-dashboard');

    // ZERO OUT css written by EpiServer
    $(targetSelector).css({
      width: '100%',
      height: 'auto',
      display: 'inline-block',
    });
    $(targetSelector)
      .parent('.nrcstat-block')
      .css({
        display: 'table',
      });

    $(targetSelector).html(
      `
      <div class="nrcstat-country-dashboard-wrapper">
        <div class="nrcstat-country-dashboard-data-block">
          <div class="nrcstat-country-dashboard-data-block__inner"></div>
        </div>
        <div class="nrcstat-country-dashboard-map-block">
          <div class="nrcstat-country-dashboard-map-block__map-container">
            <div class="nrcstat-country-dashboard-map-block__mapbox"></div>
            <div class="nrcstat-country-dsahboard-map-block__source">Kilder: UNHCR og IDMC</div>
          </div>
        </div>
      </div>`
    );
    const dataBlock = $(targetSelector).find(
      '.nrcstat-country-dashboard-data-block__inner'
    );
    const mapBlock = $(targetSelector).find(
      '.nrcstat-country-dashboard-map-block'
    );

    return { dataBlock, mapBlock };
  }

  function drawDataBlock(dataBlock, data) {
    ReactDOM.render(
      <Dashboard
        countryCode={countryCode}
        data={data}
        dataPointsToShow={dataPoints}
        onAfterRender={() => map.resize()}
      />,
      dataBlock[0]
    );
  }

  function drawMapBlock(mapBlock, data) {
    const boundingBox = leonardoCentroid.boundingbox;
    const [west, south, east, north] = boundingBox;
    const fitBounds_bounds = [[south, west], [north, east]];
    const fitBounds_config = { padding: 15 };

    const somethingWonderful = map.cameraForBounds(
      fitBounds_bounds,
      fitBounds_config
    );

    map.fitBounds(fitBounds_bounds, fitBounds_config);

    map.on('resize', () => map.fitBounds(fitBounds_bounds, fitBounds_config));

    const el = document.createElement('div');
    el.className = 'nrcstat-radial-bar-chart';

    new mapboxgl.Marker(el)
      .setLngLat(somethingWonderful.center.toArray())
      .addTo(map);

    ReactDOM.render(
      <RadialBarChart data={Object.values(dataTransformer(data))[0]} />,
      el
    );

    const singleCountry = clone(middleResolutionCountriesGeoJson);
    singleCountry.features = singleCountry.features.filter(
      c =>
        c.properties &&
        c.properties.iso_a2 &&
        c.properties.iso_a2.toUpperCase() === countryCode.toUpperCase()
    );

    map.addSource('highlight-individual-country', {
      type: 'geojson',
      data: singleCountry,
    });

    map.addLayer({
      id: 'countries-highlighted',
      type: 'fill',
      source: 'highlight-individual-country',
      paint: {
        'fill-opacity': 1,
      },
      paint: { 'fill-color': '#d4d4d4' },
    });

    map.addSource('radial-chart-title-src', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              countryLabel: 'Totaltall',
            },
            geometry: {
              type: 'Point',
              coordinates: somethingWonderful.center.toArray(),
            },
          },
        ],
      },
    });
    map.addLayer({
      id: 'radial-chart-title',
      type: 'symbol',
      source: 'radial-chart-title-src',
      layout: {
        'text-field': ['get', 'countryLabel'],
        'text-font': ['Roboto Condensed'],
        'text-max-width': 50,
        'text-size': 25,
        'text-line-height': 1,
        'text-offset': [0, 6.5],
      },
    });
    map.addSource('radial-chart-subtitle-src', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              countryLabel: 'ved inngangen til 2019',
            },
            geometry: {
              type: 'Point',
              coordinates: somethingWonderful.center.toArray(),
            },
          },
        ],
      },
    });
    map.addLayer({
      id: 'radial-chart-subtitle',
      type: 'symbol',
      source: 'radial-chart-subtitle-src',
      layout: {
        'text-field': ['get', 'countryLabel'],
        'text-font': ['Roboto Condensed'],
        'text-max-width': 50,
        'text-size': 15,
        'text-line-height': 1,
        'text-offset': [0, 12.5],
      },
    });

    //$(mapBlock).css('position', 'relative');
    //$(mapBlock).css('overflow', 'hidden');

    moveMapboxLogo(mapBlock);

    map.on('click', function(event) {
      hideTooltip();
      var selectedCountry = event.features[0].properties;

      if (mobileLegendActive || mobileShareMenuActive) {
        $(targetSelector)
          .find('.legend-container')
          .css('display', 'none');
        $(targetSelector)
          .find('#legend-button')
          .removeClass('legend-button-closed legend-button-open')
          .addClass('legend-button-closed');
        setLegendState(targetSelector);
        $(targetSelector)
          .find('.share-menu-container')
          .css('display', 'none');
        setShareMenuState(targetSelector);
        isCountryInfoPopupOrPopoverActive = false;
      } else {
        //insert Kosovo country code (has "name" but no "iso_a2" in natural earth data)
        if (selectedCountry.name == 'Kosovo') {
          selectedCountry.iso_a2 = 'KO';
        }

        //countries without iso2 code in naturalearth data have value -99 instead
        if (countryInfo__hasData(selectedCountry.iso_a2)) {
          if (isMobileDevice() && selectedCountry.iso_a2 != -99) {
            isCountryInfoPopupOrPopoverActive = true;
            countryInfo__showPopover(targetSelector, event);
          } else if (selectedCountry.iso_a2 != -99) {
            isCountryInfoPopupOrPopoverActive = true;
            countryInfo__showPopup(event, map);
          }
        }
      }
    });

    //event listener for closing legend and share menu by clicking on the non-country (e.g. sea)
    map.on('click', function(event) {
      hideTooltip();
      if (mobileLegendActive || mobileShareMenuActive) {
        $(targetSelector)
          .find('.legend-container')
          .css('display', 'none');
        $(targetSelector)
          .find('#legend-button')
          .removeClass('legend-button-closed legend-button-open')
          .addClass('legend-button-closed');
        setLegendState(targetSelector);
        $(targetSelector)
          .find('.share-menu-container')
          .css('display', 'none');
        setShareMenuState(targetSelector);
        isCountryInfoPopupOrPopoverActive = false;
      }
    });

    const hoverPopup = $(`
    <div class="global-displacement-radial-bar-chart-tooltip">
      <div class="top">2018.</div>
      <div class="data"></div>
    </div>`);
    $('body').append(hoverPopup);
    const showTooltip = countryCode => e => {
      if (isCountryInfoPopupOrPopoverActive) return;
      const dataHtml = [
        {
          color: 'rgba(114,199,231,0.72)',
          data: getCountryStat(
            countryCode,
            'idpsInXInYear',
            YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
          ).data,
        },
        {
          color: 'rgba(255,121,0,0.72)',
          data: getCountryStat(
            countryCode,
            'totalRefugeesFromX',
            YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
          ).data,
        },
        {
          color: 'rgba(253,200,47,0.72)',
          data: getCountryStat(
            countryCode,
            'newRefugeesInXFromOtherCountriesInYear',
            YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
          ).data,
        },
      ]
        .sort((a, b) => b.data - a.data)
        .map(d => {
          return { ...d, data: formatDataNumber(d.data, 'nb_NO') };
        })
        .map(
          d =>
            `<div class="line"><div class="dot" style="background-color: ${
              d.color
            }"></div>${d.data}</div></div>`
        )
        .join('\n');
      hoverPopup.children('.data').html(dataHtml);

      const newCss = {
        display: 'block',
        left:
          e.pageX + hoverPopup[0].clientWidth + 10 < document.body.clientWidth
            ? e.pageX + 10 + 'px'
            : document.body.clientWidth + 5 - hoverPopup[0].clientWidth + 'px',
        top:
          e.pageY + hoverPopup[0].clientHeight + 10 < document.body.clientHeight
            ? e.pageY + 10 + 'px'
            : document.body.clientHeight +
              5 -
              hoverPopup[0].clientHeight +
              'px',
      };
      hoverPopup.css(newCss);
    };
    function hideTooltip(e) {
      hoverPopup.css({ display: 'none' });
    }
  }

  //made popup global variable to be able to access it from setPassiveMode (needs to be removed before deactivating the map) - not sure it's a best solution??
  var countryInfo__mapboxPopup;
  var mapNavigationControl;
  var mobileLegendActive = false;
  var mobileShareMenuActive = false;
  let isCountryInfoPopupOrPopoverActive = false;

  addLegend(targetSelector);

  //change position of mapbox logo
}

//#region Country info (popover for mobile, popup for tablet/desktop)

function countryInfo__showPopover(targetSelector, event) {
  var selectedCountryIso2 = event.features[0].properties.iso_a2;
  const norwegianCountryName = norwegianCountryNames[selectedCountryIso2];

  const population = getCountryStat(
    selectedCountryIso2,
    'population',
    YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
  ).data;

  const statsTable = countryInfo__statsTable(selectedCountryIso2);

  const countryUrl = countryLinks[selectedCountryIso2];
  const countryLink = countryUrl
    ? `<p class="country-link"><a href="https://www.flyktninghjelpen.no/${countryUrl}" target="_blank">LES MER OM ${norwegianCountryName.toUpperCase()} HER</a></p>`
    : '';

  //close menu when popover opens

  $(targetSelector)
    .find('#legend-button')
    .attr('class', 'legend-button-closed');
  $(targetSelector)
    .find('.legend-container')
    .css('display', 'none');
  setLegendState(targetSelector);
  $(targetSelector)
    .find('.share-menu-container')
    .css('display', 'none');
  setShareMenuState(targetSelector);

  const popupHtml = `
      <div class="nrcstat-radialchartmap-country-info-popover-wrapper">
        <div class="popover-top-ribbon"></div>
        <span class="close-popover disable-selection"><img class="close-popup-img" src=" "></span>
        
        <div class="country-statistics">
             <p class="title">${norwegianCountryName}</p>
             <p class="population">FOLKETALL: ${population} millioner</p>
             <p class="statistics">${statsTable}</p>
             
             ${countryLink}
        </div>
      </div>
    `;

  $(targetSelector).append(popupHtml);
  $('.close-popup-img').attr('src', closeButton);
  $(
    '.nrcstat-radialchartmap-country-info-popover-wrapper .close-popover'
  ).click(() => {
    closePopover();
    isCountryInfoPopupOrPopoverActive = false;
  });
}

function closePopover() {
  $('.nrcstat-radialchartmap-country-info-popover-wrapper').remove();
}

function countryInfo__showPopup(event, map) {
  var selectedCountryIso2 = event.features[0].properties.iso_a2;
  const norwegianCountryName = norwegianCountryNames[selectedCountryIso2];
  var fullCountryName = '<h1>' + norwegianCountryName + '</h1>';
  var loader = '<div class="loader"></div>';

  countryInfo__mapboxPopup = new mapboxgl.Popup({
    closeButton: true,
    closeOnClick: true,
  })
    .setLngLat([event.lngLat.lng, event.lngLat.lat])
    .setHTML(
      '<div class="popup-container">' + fullCountryName + loader + '</div>'
    )
    .addTo(map);

  const statsTable = countryInfo__statsTable(selectedCountryIso2);

  const countryUrl = countryLinks[selectedCountryIso2];
  const countryLink = countryUrl
    ? `<p class="country-link"><a href="https://www.flyktninghjelpen.no/${countryUrl}" target="_blank">LES MER OM ${norwegianCountryName.toUpperCase()} HER</a></p>`
    : '';

  const population = getCountryStat(
    selectedCountryIso2,
    'population',
    YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
  ).data;
  const populationHtml = `<p class="population">FOLKETALL: ${population} millioner</p>`;

  countryInfo__mapboxPopup.setHTML(
    '<div class="popup-container">' +
      fullCountryName +
      populationHtml +
      statsTable +
      countryLink +
      '</div>'
  );

  countryInfo__mapboxPopup.on('close', () => {
    isCountryInfoPopupOrPopoverActive = false;
  });
}

function countryInfo__statsTable(iso2) {
  const countryStats = getCountryStats(iso2);

  let sections = [
    {
      icon: refugeesFromIcon,
      dataPoints: [
        {
          dataPointKey: 'totalRefugeesFromX',
          dataPointName: `Totalt antall flyktninger fra`,
        },
        {
          dataPointKey: 'newRefugeesFromXInYear',
          dataPointName: 'I 2018',
        },
      ],
    },
    {
      icon: refugeesToIcon,
      dataPoints: [
        {
          dataPointKey: 'refugeesInXFromOtherCountriesInYear',
          dataPointName: `Totalt antall flyktninger til`,
        },
        {
          dataPointKey: 'newRefugeesInXFromOtherCountriesInYear',
          dataPointName: 'I 2018',
        },
      ],
    },
    {
      icon: idpsIcon,
      dataPoints: [
        {
          dataPointKey: 'idpsInXInYear',
          dataPointName: 'Totalt antall internt fordrevne',
        },
        {
          dataPointKey: 'newIdpsInXInYear',
          dataPointName: 'I 2018',
        },
      ],
    },
  ];
  sections = sections.map(section => {
    section.dataPoints = section.dataPoints.map(dp => {
      let dataPointValue = getCountryStat(
        iso2,
        dp.dataPointKey,
        YEAR_TO_SHOW_IN_RADIAL_BAR_CHART
      ).data;

      if (
        includes(
          [
            'percentageWomenFleeingToCountry',
            'percentageChildrenFleeingToCountry',
          ],
          dp.dataPointKey
        )
      )
        dataPointValue = formatDataPercentage(dataPointValue, 'nb_NO');
      else dataPointValue = formatDataNumber(dataPointValue, 'nb_NO', true);

      Object.assign(dp, { dataPointValue });
      return dp;
    });
    return section;
  });

  const descriptionData = sections
    .map(
      section =>
        `<tr class="statistics-table-row">
        <td class="statistics-label"><img src="${section.icon}" /></td>
        <td align="left" class="statistics-number">
          ${section.dataPoints[0].dataPointName}: ${
          section.dataPoints[0].dataPointValue
        }<br />
          ${section.dataPoints[1].dataPointName}: ${
          section.dataPoints[1].dataPointValue
        }
        </td>
      </tr>`
    )
    .join('\n');

  const table = `<table><tbody>${descriptionData}</tbody></table>`;

  return table;
}

function countryInfo__hasData(iso2) {
  return !!_.find(countryStatsCache, c => c.countryCode === iso2);
}

function fetchData(countryCode) {
  const url = `${API_URL}/datas?filter=${encodeURIComponent(
    JSON.stringify({ where: { countryCode } })
  )}`;
  return $.getJSON(url).catch(function(err) {
    console.log(
      'error occurred during loading country stats data from loopback:'
    );
    console.log(err);
  });
}

function parseWidgetId(widgetId) {
  const patternCountryYear = /^dynamic_country_dashboard_([A-Z]{2})_(\d{4})/;
  const patternCountryYearDataPoints = /^dynamic_country_dashboard_([A-Z]{2})_(\d{4})_([A-Za-z,]+)/;

  let countryCode;
  let year;
  let dataPoints = [
    'totalRefugeesFromX',
    'idpsInXInYear',
    'refugeesInXFromOtherCountriesInYear',
    'newRefugeesFromXInYear',
    'newRefugeesInXFromOtherCountriesInYear',
    'newIdpsInXInYear',
    'voluntaryReturnsToXInYear',
    'asylumSeekersFromXToNorwayInYear',
    'population',
    'percentageWomenFleeingToCountry',
    'percentageChildrenFleeingToCountry',
  ];
  let showMap = true;

  if (patternCountryYearDataPoints.test(widgetId)) {
    const matches = patternCountryYearDataPoints.exec(widgetId);
    countryCode = matches[1];
    year = matches[2];
    dataPoints = matches[3].split(',');
  } else if (patternCountryYear.test(widgetId)) {
    const matches = patternCountryYear.exec(widgetId);
    countryCode = matches[1];
    year = matches[2];
  }

  if (widgetId.indexOf('hideMap') !== -1) {
    showMap = false;
  }

  return { countryCode, year, dataPoints, showMap };
}

function getCountryCentroid(countryCode) {
  return centroidsRaw.filter(centroid => centroid.iso === countryCode)[0];
}

function configureMapboxAccessToken() {
  mapboxgl.accessToken =
    'pk.eyJ1IjoibnJjbWFwcyIsImEiOiJjaW5hNTM4MXMwMDB4d2tseWZhbmFxdWphIn0._w6LWU9OWnXak36BkzopcQ';
}

function initializeMap(mapBlock, initialCenter) {
  const mapContainer = mapBlock.find(
    '.nrcstat-country-dashboard-map-block__mapbox'
  );

  var map = new mapboxgl.Map({
    container: mapContainer[0],
    center: initialCenter,
    style: 'mapbox://styles/nrcmaps/cjwz5szot00y61cpjqq3h9s5p',
    interactive: false,
  });

  return map;
}

function moveMapboxLogo(mapBlock) {
  mapBlock
    .find('.mapboxgl-ctrl-bottom-left')
    .removeClass('mapboxgl-ctrl-bottom-left')
    .addClass('mapboxgl-ctrl-bottom-right');
}

function addLegend(targetSelector) {
  const legend = $(targetSelector).find('.legend');

  if (isMobileDevice()) {
    addLegendMobile(legend);
  } else {
    addLegendTabletDesktop(legend);
  }

  $(targetSelector)
    .find('.legend-button-container')
    .click(function() {
      $(targetSelector)
        .find('.share-menu-container')
        .css('display', 'none');
      setShareMenuState(targetSelector);
      $(targetSelector)
        .find('.legend-container')
        .toggle();
      $(targetSelector)
        .find('#legend-button')
        .toggleClass('legend-button-closed');
      $(targetSelector)
        .find('#legend-button')
        .toggleClass('legend-button-open');
      setLegendState(targetSelector);
    });
}

const fullLegend = `
      <table>
        <tr>
            <td><span class="refugeesFrom-dot"></span></td>
            <td class="legend-text">Totalt antall flyktninger fra landet</td>
        </tr>
        <tr>
            <td><span class="refugeesTo-dot"></span></td>
            <td class="legend-text">Totalt antall flyktninger til landet</td>
        </tr>
        <tr>
            <td><span class="idps-dot"></span></td>
            <td class="legend-text">Totalt antall internt fordrevne i landet</td>
        </tr>
      </table>
      
      <p><span class="source"> Kilde: UNHCR, IDMC </span></p>
    `;

function addLegendMobile(legend) {
  $(legend).append(
    `<div class="legend-button-container">
            <a class="disable-selection">
              <div class="legend-label">&#9432;</div>
              <div id="legend-button"  class="legend-button-closed">
                <span style="color: #FF7602;">&gt;</span><span style="color: #d4d4d4;">&gt;</span>
              </div>
            </a>
          </div>
          <div id="legend-container" class="legend-container" style="display: none;"></div>
          `
  );
  $(legend)
    .find('.legend-container')
    .append($(fullLegend));
}

function addLegendTabletDesktop(legend) {
  $(legend).append(
    `<div id="legend-container" class="legend-container-desktop"></div>`
  );
  $(legend)
    .find('.legend-container-desktop')
    .append($(fullLegend));
}

//#endregion

const START_RADIUS = 0.3;
const BAR_RADIUS_WIDTH = 0.1;
const BAR_RADIUS_SPACING = 0.1;
const HELPER_BAR_RADIUS_WIDTH = 0.04;

class RadialBarChart extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  state = {
    zeroOutAllBars: true,
  };

  componentDidMount() {
    const el = this.myRef.current;
    const observer = new IntersectionObserver(
      entries => {
        entries.forEach(entry => {
          if (entry.intersectionRatio > 0.6) {
            this.setState({ zeroOutAllBars: false });
          }
        });
      },
      { threshold: 0.6, rootMargin: '0px' }
    );
    observer.observe(el);
  }

  data() {
    const maxFigure = Math.max(...this.props.data.map(v => v.figure));
    return this.props.data
      .sort((a, b) => a.figure - b.figure)
      .map((v, i) => {
        const radiusStart =
          START_RADIUS + (BAR_RADIUS_SPACING + BAR_RADIUS_WIDTH) * i;
        const radiusEnd = radiusStart + BAR_RADIUS_WIDTH;
        const helperBarRadiusStart =
          radiusStart +
          (radiusEnd - radiusStart) / 2 -
          HELPER_BAR_RADIUS_WIDTH / 2;
        const helperBarRadiusEnd =
          helperBarRadiusStart + HELPER_BAR_RADIUS_WIDTH;
        const angle = this.state.zeroOutAllBars
          ? 0
          : (v.figure / maxFigure) * 1.5 * Math.PI;
        return [
          {
            angle: angle,
            radius0: radiusStart,
            radius: radiusEnd,
            color: v.colour,
            layer: 3,
            label: v.dataLabel,
            xOffsetForDataLabel: v.xOffsetForDataLabel,
            labelWidth: v.labelWidth,
            figure: v.figure,
          },
          {
            angle: angle,
            radius0: radiusStart,
            radius: radiusEnd,
            color: v.colour,
            layer: 2,
            label: v.label,
            labelWidth: v.labelWidth,
            figure: v.figure,
          },
          {
            angle0: angle,
            angle: 1.5 * Math.PI,
            radius0: helperBarRadiusStart,
            radius: helperBarRadiusEnd,
            color: 'rgb(186,186,186,0.72)',
            layer: 1,
          },
        ];
      })
      .flat()
      .sort((a, b) => a.layer - b.layer);
  }

  render() {
    const widthHeight = 290;

    const RANGE = 6000000;

    const data = this.data();

    const maxRadius = Math.max(...data.map(v => v.radius));
    const maxFigure = Math.max(
      ...data.filter(v => v.layer === 2).map(v => v.figure)
    );

    return (
      <div ref={this.myRef}>
        {this.props.data.length > 0 && (
          <XYPlot
            className={`nrcstat-radial-chart ${this.props.data[0].iso}`}
            margin={{ left: 40, bottom: 0, top: 0, right: 40 }}
            width={widthHeight + 50}
            height={widthHeight + 50}
            xDomain={[-RANGE, RANGE]}
            yDomain={[-RANGE, RANGE]}
          >
            <ArcSeries
              animation="stiff"
              radiusDomain={[0, maxRadius]}
              data={this.data()}
              colorType="literal"
            />
            {data
              .filter(v => v.layer === 2)
              .map((v, i) => {
                //const width = getTextWidth(v.label, '"Roboto Condensed"');
                const width = v.labelWidth;
                return (
                  <LabelSeries
                    key={`${v.layer}-${i}`}
                    data={[
                      {
                        x: 0,
                        y: 0,
                        xOffset: -5,
                        yOffset: -50 + -32 * i,
                        label: v.label,
                      },
                    ]}
                    labelAnchorX="end"
                    style={{ fontFamily: 'Roboto Condensed' }}
                  />
                );
              })}
            {data
              .filter(v => v.layer === 3)
              .map((v, i) => {
                //const width = getTextWidth(v.label, '"Roboto Condensed"');
                const width = v.labelWidth;

                return (
                  <LabelSeries
                    key={`${v.layer}-${i}`}
                    data={[
                      {
                        x: 0,
                        y: 0,
                        xOffset: v.xOffsetForDataLabel,
                        yOffset: -49 + -32 * i,
                        label: v.label,
                      },
                    ]}
                    labelAnchorX="end"
                    style={{
                      fontFamily: 'Roboto Condensed',
                      fontWeight: 'bold',
                      fontSize: '16px',
                    }}
                  />
                );
              })}
          </XYPlot>
        )}
      </div>
    );
  }
}

const dataPointToLabel = {
  idpsInXInYear: 'XXX internt fordrevne',
  totalRefugeesFromX: 'XXX flyktninger fra',
  refugeesInXFromOtherCountriesInYear: 'XXX flyktninger til',
};
const dataPointToColour = {
  idpsInXInYear: 'rgba(114,199,231,0.72)',
  totalRefugeesFromX: 'rgba(255,121,0,0.72)',
  refugeesInXFromOtherCountriesInYear: 'rgba(253,200,47,0.72)',
};

const dataTransformer = data => {
  const filtered = data.filter(v =>
    includes(
      [
        'idpsInXInYear',
        'totalRefugeesFromX',
        'refugeesInXFromOtherCountriesInYear',
      ],
      v.dataPoint
    )
  );
  const countries = groupBy(filtered, 'countryCode');
  const mapped = mapValues(countries, countryDatas =>
    countryDatas.map(countryData => {
      const label = dataPointToLabel[countryData.dataPoint].replace(
        'XXX',
        isNull(countryData.data) ? '' : ''
      );
      const dataLabel = isNull(countryData.data)
        ? '-'
        : formatDataNumber(countryData.data, 'nb_NO', true);
      let xOffsetForDataLabel;
      switch (countryData.dataPoint) {
        case 'totalRefugeesFromX':
          xOffsetForDataLabel = -75;
          break;

        case 'refugeesInXFromOtherCountriesInYear':
          xOffsetForDataLabel = -73;
          break;

        case 'idpsInXInYear':
          xOffsetForDataLabel = -88;
          break;

        default:
          xOffsetForDataLabel = 0;
          break;
      }
      return {
        dataLabel: dataLabel,
        xOffsetForDataLabel: xOffsetForDataLabel,
        label: label,
        figure: countryData.data,
        iso: countryData.countryCode,
        colour: dataPointToColour[countryData.dataPoint],
      };
    })
  );
  return mapped;
};

function getCountryStats(data, countryIso2Code) {
  return data.filter(c => c.countryCode === countryIso2Code);
}

function getCountryStat(rawData, countryCode, dataPoint, year) {
  const stats = rawData.filter(
    c => c.countryCode === countryCode && c.year === year
  );
  if (!stats) return null;
  const data = stats.filter(d => d.dataPoint === dataPoint);
  if (data && data.length > 0) return data[0];
  return null;
}

export { drawCountryDashboard };
