
## Todo list

* Split `webpack/base.config.js` into `dev` and `prod`. Analyze [this](https://webpack.js.org/configuration/devtool/#components/sidebar/sidebar.jsx) while doing so.
* Finish reading through the four contentful articles to find further ways to improve webpack
* Implement lazy loading according to widget needs: https://webpack.js.org/guides/lazy-loading/. Make sure to read and use tree shaking, code splitting and caching as well. https://medium.com/front-end-hacking/lazy-loading-with-react-and-webpack-2-8e9e586cf442 
* Reread this and implement: https://slack.engineering/keep-webpack-fast-a-field-guide-for-better-build-performance-f56a5995e8f1
* Implement semver
* Review all [plugins](https://webpack.js.org/plugins/extract-text-webpack-plugin/) and consider their use in the wlib
* Set up the SplitChunksPlugin: https://auth0.com/blog/webpack-4-release-what-is-new/. Make sure that all vendors are in their own file. https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
* Set up aliases! https://webpack.js.org/configuration/resolve/


