const webpack = require('webpack')
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.config.js');

module.exports = merge(baseConfig, {
  plugins: [
    new webpack.DefinePlugin({
      LIB_URL: JSON.stringify('https://wlib.nrcdata.no/')
    })
  ]
});
