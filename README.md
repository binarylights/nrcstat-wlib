
# NrcStatWLib

## Introduction

## Usage
Use `npm start` for development mode
Use `npm build` to build the library into production mode

## Some scripts are configured in package.json:
start: `webpack-dev-server --config './webpack/dev.config.js' --mode=development --progress`

build: `webpack  --config './webpack/prod.configjs' --mode=production --progress`

analyze: `webpack --config './webpack/prod.config.js' --mode=production --progress --profile --json > stats.json && webpack-bundle-analyzer stats.json`

## Configuration summary

* Babel compiles down from ES6 to ES5 through the presets 'env' and 'stage-2'. Config for this in .babelrc file.
* letsencrypt is used on wlib.nrcdata.no - it must be renewed every 3 months! 

## About development and production modes

Recall as per https://webpack.js.org/concepts/mode/ that development and production modes auto-enables some plugins:
- in development mode: NamedChunksPlugin and NamedModulesPlugin
- in production mode: FlagDependencyUsagePlugin, FlagIncludedChunksPlugin, ModuleConcatenationPlugin,
  NoEmitOnErrorsPlugin, OccurrenceOrderPlugin, SideEffectsFlagPlugin and UglifyJsPlugin

## History of NrcStatWlib
This git repository, nrcstat-wlib, is a migration from older nrc-stat-widget-library-v2. The older library used
webpack v2 (instead of v4 in the new lib), and the webpack configuration was sloppy and not fully done.

## Loaders used
1. [css-loader](https://github.com/webpack-contrib/css-loader), with an application seen [here](https://stackoverflow.com/questions/44474484/how-to-import-css-files-into-webpack)

## Plugins used
1. AggressiveMergingPlugin

## Lessons learned that have been implemented in this lib:
##### 1. Prefer named exports over defaults exports:

instead of:

```
function addTwoNumbers(num1, num2)
export default addTwoNumbers
```

do this:

`export { addTwoNumbers }`

Lesson learned here: https://blog.neufund.org/why-we-have-banned-default-exports-and-you-should-do-the-same-d51fdc2cf2ad

##### 2. There are some easy webpack "tricks" that quickly add value

Invoke webpack with `--progress` or `--colors`

Resource: https://github.com/rstacruz/webpack-tricks

##### 3. Webpack config should be split according to environment
The result is webpack/base.config.js, prod.config.js and dev.config.js

Resource: https://simonsmith.io/organising-webpack-config-environments/

##### 4. Various stuff about webpack config directives: see inline comments in webpack config files for more info!

##### 5. Webpack can work with babel to compile JS code

Resource: https://www.robinwieruch.de/minimal-react-webpack-babel-setup/

##### 6. The loader [`imports-loader`](https://webpack.js.org/loaders/imports-loader/) is crucial in handling old modules ...

... such as datatables.

##### 7. `sideEffects: false` is crucial for tree shaking

... as learned here: https://github.com/webpack/webpack/issues/6065#issuecomment-351060570

> A "side effect" is defined as code that performs a special behavior when imported, other than exposing one or more exports. An example of this are polyfills, which affect the global scope and usually do not provide an export.

(https://webpack.js.org/guides/tree-shaking/)

##### 8. Webpack rules for authoring libraries!

This guide was repeatedly used: https://webpack.js.org/guides/author-libraries/

##### 9. Bundle analysis is awesome!

https://hackernoon.com/optimising-your-application-bundle-size-with-webpack-e85b00bab579

##### 10. The MDN Mozilla Developer Network is awesome as usual, so is its explanation of `export`

>```
>export { name1, name2, …, nameN };
>export { variable1 as name1, variable2 as name2, …, nameN };
>export let name1, name2, …, nameN; // also var, const
>export let name1 = …, name2 = …, …, nameN; // also var, const
>export function FunctionName(){...}
>export class ClassName {...}
>
>export default expression;
>export default function (…) { … } // also class, function*
>export default function name1(…) { … } // also class, function*
>export { name1 as default, … };
>
>export * from …;
>export { name1, name2, …, nameN } from …;
>export { import1 as name1, import2 as name2, …, nameN } from …;
>export { default } from …;
(https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export)

# Resources

* A series of articles from ContentFul about Webpack
** https://www.contentful.com/blog/2017/10/10/put-your-webpack-on-a-diet-part-1/
** https://www.contentful.com/blog/2017/10/19/put-your-webpack-bundle-on-a-diet-part-2/
** https://www.contentful.com/blog/2017/10/27/put-your-webpack-bundle-on-a-diet-part-3/
** https://www.contentful.com/blog/2017/11/13/put-your-webpack-bundle-on-a-diet-part-4/
* Sitepoint guide: https://www.sitepoint.com/beginners-guide-webpack-module-bundling/
* Stuff that was new in webpack 4: https://medium.com/webpack/webpack-4-released-today-6cdb994702d4
* About hot reloading: https://blog.cloudboost.io/live-reload-hot-module-replacement-with-webpack-middleware-d0a10a86fc80
* Great ideas for pushing optimization and building as far as possible: https://slack.engineering/keep-webpack-fast-a-field-guide-for-better-build-performance-f56a5995e8f1
* Yet another few ways of reducing final bundle size .... https://swizec.com/blog/dirty-hack-took-30percent-off-webpack-size/swizec/7657


